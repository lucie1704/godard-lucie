#include <iostream>

using namespace std;

struct Noeud{
    int donnee; 
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
    string name;
};

struct DynaTableau{
    int* donnees; //ceci est un array
    // your code
    int capacite;
    int nbElements;
};


void initialise(Liste* liste)
{
    liste->premier=nullptr;
    // liste->name= name;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier==nullptr) {
        return true;
    }
    else{
        return false;
    }
}

void ajoute(Liste* liste, int valeur) //ajoute valeur à la fin de la liste et rajouter de la place si plus de place
{
    Noeud* noeud = new Noeud; //crée un nouveau noeud dans la liste 
    noeud->donnee=valeur; //la donnée de ce nouveau noeud devient la valeur qu'on veut ajouter
    if (liste->premier==nullptr)//si on a rien dans le premier élément de la liste cest là qu'on ajoute notre valeur
    {
        liste->premier=noeud; 
    }
    else{
        Noeud* next = liste->premier; 
        while(next->suivant!=nullptr){//on parcourt les éléments de la liste jusqu'à trouver le null et c'est là ou on va ajouter notre noeud
            next=next->suivant;
        }
        next->suivant= noeud;  
    }
    noeud->suivant=nullptr; //après qu'on ait ajouté un élément on indique que celui d'après est nullptr pour finir la liste

}

void affiche(const Liste* liste)
{
    // cout << liste->name << " : ";
    Noeud* next = liste->premier;
    while(next!= nullptr){
        cout << next->donnee << ", ";
        next=next->suivant;
    }
    cout << endl;
}

int recupere(const Liste* liste, int n)
{
    int index=0;
    int recup=-1;
    Noeud* element = liste->premier; 
    while(element!= nullptr){
        if(index==n){
            recup=element->donnee;
            break;
        }
        else{
            index++;  
            element=element->suivant;
        }
    }
    return recup;
}

int cherche(const Liste* liste, int valeur)
{
    int index=0;
    bool trouve=false;
    Noeud* next = liste->premier; 

    while(next!= nullptr){//on parcourt toute la liste
        if(next->donnee==valeur){ //si on trouve la valeur trouve est vrai et on sort de la boucle
            trouve=true;
            break;
        }
        else{
            index++; //sinon on incrémente l'index 
            next=next->suivant;//on change next pour re tester le prochain élément de la liste dans le while
        }
    }

    if(trouve){ //vérifie si on a trouvé ou pas
        return index;
    }
    else{
        return -1;
    }        
}

void stocke(Liste* liste, int n, int valeur)//remplace une valeur de la liste à l'indice n par valeur
{
    int index=0;
    Noeud* element = liste->premier; 
    while(element!= nullptr){
        if(index==n){
            element->donnee=valeur;
            break;
        }
        else{
            index++;  
            element=element->suivant;
        }
    }
}

void ajoute(DynaTableau* tableau, int valeur)
{
    tableau->donnees[tableau->nbElements]= valeur;
    tableau->nbElements++;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite= capacite;
    tableau->donnees = (int*) malloc (sizeof(int)*tableau->capacite);//on réserve de la place dans notre tableau
    tableau->nbElements = 0;
}

bool est_vide(const DynaTableau* tableau)
{
    return (tableau->nbElements==0);
}

void affiche(const DynaTableau* tableau)
{
    for (int i=0; i<tableau->nbElements; i++){
        cout << tableau->donnees[i]<< ", ";
    }
    cout << endl;
}

int recupere(const DynaTableau* tableau, int n)
{
    if (n<tableau->nbElements && n>=0){ //si l'indice n existe
        return tableau->donnees[n];
    }
    else {
        return -1;
    }

}

int cherche(const DynaTableau* tableau, int valeur)
{
    for (int i=0; i<tableau->nbElements; i++){
        if(tableau->donnees[i]==valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n]=valeur;
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    int recup = liste->premier->donnee;
    liste->premier=liste->premier->suivant;//on décale toutes les valeurs en remplaçant juste le premier par son suivant
    //comme chaque élément point sur le suivant si on dit au deuxieme de passer premier il passe premier et tous les liens vers les suivants sont conservés 
    return recup;
}

//PILE
// pas réussi à ajouter à la fin et à retirer à la fin donc j'ai fait ajouté au début en décalant le reste 
// et récupérer le premier en redécalant dans lautre sens

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    //on ajoute au début
    Noeud* first = new Noeud;
    first->donnee = valeur;
    first->suivant = liste->premier;
    liste->premier= first;    
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste) 
{
    //on recup et retire le premier
    if (est_vide(liste)){
        return -1;
    }
    else{
        int recup = liste->premier->donnee;
        liste->premier=liste->premier->suivant;
        return recup;
    }

}


int main()
{
    
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste a " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste a " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements apres stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    cout << "TEST PILE ET FILE"<<endl;
    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
    
}
