#include "tp1.h"
#include <QApplication>
#include <time.h>


int isMandelbrot(Point z, int n, Point point){
    // recursiv Mandelbrot
    // ça marche !!!
    // check n
    if (n==0){
        return 0;
    }
    else if (n>0){
        //calculer z²+c
        float monReel = z.x; //valeur provisoire vu quon change sa valeur mais qu'on en a besoin pour calculer z.y
        z.x=pow(z.x, 2)-pow(z.y, 2)+point.x; // la partie réelle de z²+c c'est x² - y² + partie réelle de point
        z.y=2*monReel*z.y+point.y; //la partie imaginaire de z²+c c'est 2*x*y+ partie imaginaire de point  

        float module = sqrt(pow(z.x,2)+pow(z.y,2)); //calcul du module de z²+c

        if (module<2) //Alors le point est compris dans l'espace de Mandelbrot
        {
            return isMandelbrot(z, n-1, point); //récursivité en décrémentant n
        }
        else{
            return 1;
        }
    }
    else {
        return -1;
    }


    // check length of z
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



