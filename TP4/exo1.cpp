#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;//taille du tableau Heap
	this->get(i) = value;
	while (i>0 && this->get(i)>this->get((i-1)/2)){
		this->swap(i, (i-1)/2);
		i=((i-1)/2);
	}
}

void Heap::heapify(int heapSize, int nodeIndex)//premier nodeIndex=0 on commence en haut du tas
{
	// use (*this)[i] or this->get(i) to get a value at index i

	//heapify permet de faire un premier tri du tableau et notamment de mettre la plus grande valeur tout en haut
	
	int i_max = nodeIndex; //parent au début

	int left = leftChild(nodeIndex);
	int right = rightChild(nodeIndex);

	if(left<heapSize && get(left)>get(i_max)){
		i_max = left;
	}

	if(right<heapSize && get(right)>get(i_max)){
		i_max = right;
	}

	if(i_max!=nodeIndex){
		swap(nodeIndex, i_max);
		heapify(heapSize, i_max);
	}

	
}

void Heap::buildHeap(Array& numbers)
{
	//construit un tas à partir du tableau numbers
	for(int i=0; i<numbers.size();i++){//on parcourt le tableau de numbers
		insertHeapNode(numbers.size(), numbers[i]);//on insert cette valeur dans un nouveau noeud du tas
	}
}

void Heap::heapSort()
{
	//trie le tableau de valeurs de la plus petite à la plus grande
	//soit heap[0]=plus petite valeur et heap[size-1]=plus grande valeur

	for (int i = size()/2 - 1; i >= 0; i--){
        heapify(size(), i);
	}

	for(int i=size()-1; i>=0 ; i--){
		swap(0, i);
		heapify(i, 0);
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
