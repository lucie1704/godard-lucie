#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child

        if (value < this->value && left!=nullptr){
            left->insertNumber(value);
        }
        else if (value > this->value && right!=nullptr){
            right->insertNumber(value);
        }
        else{
            if(value<this->value){
                left = new SearchTreeNode(value);
            }
            else{
                right = new SearchTreeNode(value);
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int maxLeft = (left==nullptr)?0 : left->height()+1; 
        int maxRight = (right==nullptr)?0 : right->height()+1; 

        if(this->isLeaf()){ 
            return 1;
        }
        else{ 
            return std::max(maxLeft, maxRight);
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int maxLeft = (left==nullptr)?0 : left->nodesCount(); 
        int maxRight = (right==nullptr)?0 : right->nodesCount(); 

        if(this->isLeaf()){ 
            return 1;
        }
        else{ 
            return maxLeft+maxRight+1;
        }

	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        return (left==nullptr && right==nullptr); //to true if condition fulfilled and faulse otherwise
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        if(this->isLeaf()){ 
            leaves[leavesCount]=this;
            leavesCount++;
        }
        else{ 
            if (left!=nullptr){
                left->allLeaves(leaves, leavesCount); 
            }
            if (right!=nullptr){
                right->allLeaves(leaves, leavesCount); 
            }
        }

        
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
