#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    //ça marche okéé
    int min; 
    for (int i = 0; i < toSort.size(); i++)
    {
        min = i; //stock l'indice 
        
        for (int j=i+1; j<toSort.size(); j++) //on fait commencer j à i+1 pour pas recomparer avec tout le tableau à chaque fois ni comaprer toSort[i] avec lui meme
        {
            if (toSort[j] < toSort[min]) //est-ce que la valeur est plus petite que ma dernière plus petite valeur (initialement toSort[i])
            min = j; //si oui ça devient la min
        }

        if(toSort[i]!=toSort[min])
        toSort.swap(i, min); //on swap le plus petit avec le premier si c'est pas le premier lui même le plus petit 
    }
    
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
