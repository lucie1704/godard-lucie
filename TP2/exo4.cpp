#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

//QuickSort
//ça marche mais voir si pas possible d'optimiser 

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	//jai pas capté ct quoi ça
	
	// Array& sorted = w->newArray(size);
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes

	// split
	int pivot=toSort[0];
	for (int i = 1; i < size && toSort[i]!=-1; i++)//parcourt toSort / on commence à 1 pour pas inclure le pivot 
	{
		if (toSort[i]<pivot)//rempli lower avec tout ce qui est plus petit que pivot
		{
			lowerArray[lowerSize]=toSort[i];
			lowerSize++;
		}
		else {//rempli geater avec tout ce qui est plus grand que pivot
			greaterArray[greaterSize]=toSort[i];
			greaterSize++;
		}
	}
	
	// recursiv sort of lowerArray and greaterArray
	if(lowerSize>0)//condition pour sortir de la récursivité (quand on a des tableaux à 0 donc qu'il y a qu'un élément à trier)
	recursivQuickSort(lowerArray, lowerSize);
	if(greaterSize>0)
	recursivQuickSort(greaterArray, greaterSize);

	// merge
	for (int i = 0; i <lowerSize ; i++){//on rempli toSort en commençant par les lower
		toSort[i]=lowerArray[i];		
	}
	toSort[lowerSize]=pivot; // + pivot

	// + les greater / ici boucle avec 2 variables, i pour continuer le remplissage de toSort juste après le pivot
	// et j pour récupérer les valeurs de greater
	int j=0;
	for (int i=lowerSize+1; i<size; i++){
		toSort[i]=greaterArray[j];
		j++;
	}

}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
